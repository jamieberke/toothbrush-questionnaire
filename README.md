# Toothbrush Questionnaire

#### Description
This component is a questionnaire to find out which toothbrush you should use. It is built using create-react-app. It uses mock API's from mocky. If the API's fail, you can switch out the response for the mock data inside `src/mocks`. Although the project is built for a toothbrush questionnaire, it can in practice take any set of question + answer data as structured below:

```
[
  {
    id: "1",
    question: "Do you like cheese?",
    answers: [
      {
        answerText: "Yes",
        type: "Pizza",
      },
      {
        answerText: "No",
        type: "Sandwiches",
      },
    ],
  }
]
```

The app calcualtes the final decision by accumulating a count of the `type` property. The above example asks users questions to determine their favourite food.

#### How to use the demo

1. Access the project route
2. Run `npm install`
3. Run `npm run start`