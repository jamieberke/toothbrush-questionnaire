import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { Button } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Quiz from "./components/Quiz";
import appStyles from "./App.module.scss";

const App = () => {
  const [quizActive, toggleQuizActive] = useState(false);

  return (
    <div className={appStyles.app}>
      <Typography variant="h4" component="h1" gutterBottom>
        Toothbrush Questionnaire
      </Typography>
      <Card variant="outlined">
        <div>
          {quizActive ? (
            <Quiz />
          ) : (
            <CardContent className={appStyles["app__intro"]}>
              <p>
                This is a quiz to determine which toothbrush you should use!
              </p>

              <Button
                variant="contained"
                color="primary"
                onClick={() => toggleQuizActive(!quizActive)}
              >
                Start
              </Button>
            </CardContent>
          )}
        </div>
      </Card>
    </div>
  );
};

export default App;
