import { Button, ButtonGroup } from "@material-ui/core";

const Answers = ({ data, onClick }) => (
  <ButtonGroup variant="text" color="primary" aria-label="Group of answers">
    {data.map((answer, index) => (
      <Button
        onClick={() => onClick(answer.type)}
        key={`${answer.answerText}_${index}`}
      >
        {answer.answerText}
      </Button>
    ))}
  </ButtonGroup>
);

export default Answers;
