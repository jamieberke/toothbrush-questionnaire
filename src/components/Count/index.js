import Typography from "@material-ui/core/Typography";
import countStyles from "./styles.module.scss";

const Count = ({ question, totalQuestions }) => (
  <Typography className={countStyles.count} variant="caption">
    Question {question} of {totalQuestions}
  </Typography>
);

export default Count;
