import Typography from "@material-ui/core/Typography";

const Question = ({ question, number }) => (  
  <Typography variant="subtitle1" component="h2" gutterBottom>
    {number}. {question}
  </Typography>
);

export default Question;
