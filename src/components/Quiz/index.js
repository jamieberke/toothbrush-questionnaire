import { useFetch } from "../../hooks/fetch";
import CONSTANTS from "../../constants";
import Answers from "../Answers";
import Question from "../Question";
import Results from "../Results";
import Count from "../Count";
import { getCSSVar } from "./../../utils/css";
import React, { useState } from "react";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import quizStyles from "./styles.module.scss";

const Quiz = () => {
  const { data, error } = useFetch(CONSTANTS.APIS.QUESTIONS_API);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [answersCount, setAnswersCount] = useState({});
  const [animateType, setAnimateType] = useState("");
  const [animateDirection, setAnimateDirection] = useState("");
  const animationMappings = {
    animateIn: "quiz-animate-in",
    animateOut: "quiz-animate-out",
    animateReverse: "--reverse",
    animationDuration: parseFloat(getCSSVar("--animation-time")) * 1000,
  };
  const {
    animateIn,
    animateOut,
    animateReverse,
    animationDuration,
  } = animationMappings;

  const reset = () => {
    setCurrentQuestion(0);
    setAnswersCount({});
    setAnimateType("");
    setAnimateDirection("");
  };

  const nextQuestion = (type) => {
    setAnimateType(animateOut);
    setAnimateDirection("");
    setTimeout(
      () => {
        setCurrentQuestion(currentQuestion + 1);
        setAnswersCount({
          ...answersCount,
          [currentQuestion]: type,
        });
        setAnimateType(animateIn);
      },
      currentQuestion === data.length - 1 ? 0 : animationDuration
    );
  };

  const previousQuestion = () => {
    if (animateType === animateOut) {
      return;
    }
    setAnimateType(animateOut);
    setAnimateDirection(animateReverse);
    setTimeout(() => {
      const newAnswersCount = {
        ...answersCount,
      };
      delete newAnswersCount[currentQuestion];
      setCurrentQuestion(currentQuestion - 1);
      setAnswersCount(newAnswersCount);
      setAnimateType(animateIn);
      setAnimateDirection(animateReverse);
    }, animationDuration);
  };

  return (
    <div className={quizStyles.quiz}>
      {!error ? (
        data.length ? (
          data[currentQuestion] ? (
            <>
              <CardContent>
                <Count
                  question={currentQuestion + 1}
                  totalQuestions={data.length}
                />
              </CardContent>
              <div
                className={`${quizStyles[animateType]} ${quizStyles[animateDirection]}`}
              >
                <CardContent>
                  <Typography
                    component={"span"}
                    color="textSecondary"
                    gutterBottom
                  >
                    <Question
                      number={currentQuestion + 1}
                      question={data[currentQuestion].question}
                    />
                  </Typography>
                </CardContent>
                <CardActions className={quizStyles["quiz__answers"]}>
                  <Answers
                    data={data[currentQuestion].answers}
                    onClick={nextQuestion}
                  />
                </CardActions>
              </div>
              {currentQuestion > 0 && (
                <CardActions>
                  <Typography variant="overline" display="block">
                    <Link href="#" onClick={previousQuestion}>
                      &lt;&lt; Previous
                    </Link>
                  </Typography>
                </CardActions>
              )}
            </>
          ) : (
            <Results results={answersCount} restart={reset} />
          )
        ) : (
          <CardContent>
            <p>Loading...</p>
          </CardContent>
        )
      ) : (
        <CardContent>
          <Typography component={"span"}>
            Something has gone wrong. Please try again alter
          </Typography>
        </CardContent>
      )}
    </div>
  );
};

export default Quiz;
