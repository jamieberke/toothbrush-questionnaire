import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import { getResults } from "./../../utils/results";
import resultsStyles from "./styles.module.scss";

const Results = ({ results, restart }) => {
  const quizResults = getResults(results);
  return (
    <>
      <CardContent>
        <span>Result: {quizResults.join(" & ")}</span>
      </CardContent>
      <CardActions className={resultsStyles["results__restart"]}>
        <Typography variant="overline" display="block">
          <Link href="#" onClick={restart}>
            Restart
          </Link>
        </Typography>
      </CardActions>
    </>
  );
};

export default Results;
