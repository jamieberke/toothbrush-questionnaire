const CONSTANTS = {
  APIS: {
    QUESTIONS_API:
      "https://run.mocky.io/v3/9d6e3324-1a34-4fb2-92e9-1afd572c17c0",
    TEST_API: "https://run.mocky.io/v3/8f13fc42-59fa-4337-a3a0-cdca38d00fa0"
  },
};

export default CONSTANTS;
