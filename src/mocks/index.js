const MOCKS = {
  APIS: {
    QUESTIONS_API: [
      {
        id: "1",
        question: "How many times do you brush your teeth a day?",
        answers: [
          {
            answerText: "Once",
            type: "Oral B Genius 8000",
          },
          {
            answerText: "Twice",
            type: "Oral B Pro 2",
          },
          {
            answerText: "Three Times",
            type: "Oral B i09",
          },
        ],
      },
      {
        id: "2",
        question: "How long do you brush your teeth for?",
        answers: [
          {
            answerText: "<1 minute",
            type: "Oral B Smart 4",
          },
          {
            answerText: "1-2 minutes",
            type: "Oral B Genius 8000",
          },
          {
            answerText: "2-3 minutes",
            type: "Oral B Pro 2",
          },
        ],
      },
      {
        id: "3",
        question: "Do you use an electric toothbrush?",
        answers: [
          {
            answerText: "Yes",
            type: "Oral B i09",
          },
          {
            answerText: "No",
            type: "Oral B Genius 8000",
          },
        ],
      },
      {
        id: "4",
        question: "What type of toothbrush head do you usually use?",
        answers: [
          {
            answerText: "Soft",
            type: "Oral B i09",
          },
          {
            answerText: "Medium",
            type: "Oral B Smart 4",
          },
          {
            answerText: "Firm",
            type: "Oral B Genius 8000",
          },
        ],
      },
      {
        id: "5",
        question: "Do you experience bleeding gums when brushing?",
        answers: [
          {
            answerText: "Yes",
            type: "Oral B Genius 8000",
          },
          {
            answerText: "No",
            type: "Oral B Pro 2",
          },
        ],
      },
      {
        id: "6",
        question: "Do you experience teeth sensitivity?",
        answers: [
          {
            answerText: "Yes",
            type: "Oral B i09",
          },
          {
            answerText: "No",
            type: "Oral B Genius 8000",
          },
        ],
      },
    ],
    TEST_API: [
      {
        id: "1",
        question: "Question one?",
        answers: [
          {
            answerText: "Yes",
            type: "Type 1",
          },
          {
            answerText: "No",
            type: "Type 2",
          },
        ],
      },
      {
        id: "2",
        question: "Question two?",
        answers: [
          {
            answerText: "Yes",
            type: "Type 1",
          },
          {
            answerText: "No",
            type: "Type 2",
          },
        ],
      },
      {
        id: "3",
        question: "Question three?",
        answers: [
          {
            answerText: "Yes",
            type: "Type 1",
          },
          {
            answerText: "No",
            type: "Type 2",
          },
        ],
      },
      {
        id: "4",
        question: "Question four?",
        answers: [
          {
            answerText: "Yes",
            type: "Type 1",
          },
          {
            answerText: "No",
            type: "Type 2",
          },
        ],
      },
    ],
  },
};

export default MOCKS;
