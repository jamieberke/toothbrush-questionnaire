const generateAnswerCounts = (results) => {
  return Object.values(results).reduce((acc, curr) => {
    typeof acc[curr] == "undefined" ? (acc[curr] = 1) : (acc[curr] += 1);
    return acc;
  }, {});
};

const getResults = (results) => {
  const counts = generateAnswerCounts(results);
  return Object.keys(counts).filter(
    (item) => counts[item] === Math.max(...Object.values(counts))
  );
};

export { getResults }